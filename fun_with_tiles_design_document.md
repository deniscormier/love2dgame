# Design Document: Fun with Tiles!

## Game States
Main Menu
Gameplay
Pause
Game Over
How to Play (Pop-up)
High Scores (Pop-up)
Exit

### State transitions

Start button:
Main Menu -> Gameplay

Pause button:
Gameplay -> Pause

How to play button:
Main Menu -> How to play (remember last state)
Pause -> How to play (remember last state)

Close button:
How to play -> Main Menu|Pause
High Scores -> Main Menu

Game over condition (timer hits 0:00):
Gameplay -> Game Over

Time elapsed (3 seconds)
Game Over -> High Scores

High Score button:
Main Menu -> High Scores

Exit to Menu button:
Pause -> Main Menu

Exit button:
Main Menu -> Exit

## Screen descriptions

Main Menu

- Show game title

How to Play

- Show how to play
- Show information on different game modes

Gameplay

- Drag a tile on another to swap them
    - While dragging, have the block under the mouse appear semi-transparently in the freed space (preview of the swap's result)
- A swap that doesn't form lines of three should be reverted
- Any threes, fours, or fives are removed and replaced with random blocks
    - The animation to spawn the blocks should be quick, but noticeable
- Timer
    - A red meter decreases as the time gets closer to 0
    - (not implemented) It would be neat if the meter got brighter too.
- Scoring
    - Row of three/four/five/six: 30/45/70/110
    - No bonuses for clearing several rows at the same time (for the moment)
    - The points earned are shown as the row is cleared
        - Show "+ X" beside the total
    - The points are added to the total
    - A blue meter increases as the score gets closer to the goal (Arcade mode + Time Attack)
    - (not implemented) It would be neat if the meter got brighter too.
- Several modes
    - Score Attack: Get to X points as quickly as possible
        - 2000
    - Time Attack: Get the most points in X minutes
        - 2:00
    - (not implemented) Arcade Mode
        - Preconfigured starting positions
        - Three (30, 45, 60, ...) : 15 + 15 * level
        - Four (45, 65, 85, ...) : 25 + 20 * level
        - Five (60, 85, 110, ...) : 35 + 25 * level
        - Level X must be cleared with Y points in Z minutes

Game Over

- Using a pop-up, prompt the player for a name when a score cracks the top 10 in high scores

High scores

- Show top 10 for Score Attack mode for each score limit
- Show top 10 for Time Attack mode for each time limit

## Idea for a possible variant

- Colors are replaced by numbers. A target number is shown, and you must form a row/column that meets that number
