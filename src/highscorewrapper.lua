-- Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
-- See the file license.txt for copying permission.

local GLOBAL = require("globalvars")
local class = require("lib.30log")
local high_score = require("lib.sick")

HighScoreWrapper = class("HighScoreWrapper", {
    bronze_star = love.graphics.newImage(GLOBAL.IMAGES_DIR .. "starBronze.png"),
    silver_star = love.graphics.newImage(GLOBAL.IMAGES_DIR .. "starSilver.png"),
    gold_star = love.graphics.newImage(GLOBAL.IMAGES_DIR .. "starGold.png")
})

function HighScoreWrapper:init()
    self.high_score_tables = {}
end

function HighScoreWrapper:addfile(filename_p, category, slots_p, name, scores)
    if #scores == slots_p then
        if category == "Time" then
            --high_score.set(filename, slots, "Ascending", name, scores)
            self.high_score_tables[category] = {
                filename = filename_p,
                slots = slots_p,
                sort_order = "Ascending"
            }
        elseif category == "Score" then
            --high_score.set(filename, slots, "Descending", name, scores)
            self.high_score_tables[category] = {
                filename = filename_p,
                slots = slots_p,
                sort_order = "Descending"
            }
        else
            logger:error("An invalid category '" .. category .. "' was assigned to the HighScoreWrapper object")
        end
        -- We could be setting it for the first time
        high_score.set(
            self.high_score_tables[category].filename,
            self.high_score_tables[category].slots,
            self.high_score_tables[category].sort_order,
            name,
            scores
        )
        high_score:save()
    else
        GLOBAL.logger:error("Cannot instantiate the high scores, the amount of provided names or scores does not match the number of slots")
    end
end

function HighScoreWrapper:draw(category)
    love.graphics.setFont(mainFontLarge)
    love.graphics.print("High Score Tables", 20, 20);
    love.graphics.print(GLOBAL.HIGH_SCORE_MODE, 20, 130);
    love.graphics.setFont(mainFont)

    love.graphics.print("Rank", 20, 180)
    love.graphics.print("Name", 100, 180)
    love.graphics.print("Score", 250, 180)

    love.graphics.draw(self.gold_star, 30, 210, 0, 0.75)
    love.graphics.draw(self.silver_star, 30, 240, 0, 0.75)
    love.graphics.draw(self.bronze_star, 30, 270, 0, 0.75)

    high_score.set(
        self.high_score_tables[category].filename,
        self.high_score_tables[category].slots,
        self.high_score_tables[category].sort_order
    )

    for i, score, name in high_score() do
        love.graphics.print(i, 20, i * 30 + 200)
        love.graphics.print(name, 100, i * 30 + 200)
        if category == "Time" then
            -- Figure out if a leading zero has to be printed (done if seconds < 10)
            local seconds = score % 60
            local leading_zero = ""
            if (seconds < 10) then
                leading_zero = "0"
            end
            love.graphics.print(math.floor(score / 60) .. ":" .. leading_zero .. math.floor(seconds * 100) / 100, 250, i * 30 + 200);
        else
            love.graphics.print(score, 250, i * 30 + 200)
        end
    end
end

function HighScoreWrapper:add(category, name, score)
    high_score.set(
        self.high_score_tables[category].filename,
        self.high_score_tables[category].slots,
        self.high_score_tables[category].sort_order
    )
    high_score.add(name, score)
    high_score.save()
end

function HighScoreWrapper:ishighscore(category, score)
    high_score.set(
        self.high_score_tables[category].filename,
        self.high_score_tables[category].slots,
        self.high_score_tables[category].sort_order
    )

    score_to_compare_with = nil
    for i, score, name in high_score() do
        score_to_compare_with = score;
    end

    if category == "Time" then
        if score < score_to_compare_with then
            return true
        end
    elseif category == "Score" then
        if score > score_to_compare_with then
            return true
        end
    end
    return false
end