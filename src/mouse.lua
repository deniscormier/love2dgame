-- Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
-- See the file license.txt for copying permission.

local class = require("lib.30log")

Mouse = class("Mouse", {
    held_block = nil,
    swap_block = nil
})

-- TODO see if it can be omitted
function Mouse:init()

end

function Mouse:holdblock(block_index, x_index, y_index)
    self.held_block = {
        block = block_index,
        x = x_index,
        y = y_index
    }
end

function Mouse:isholdingblock()
    return self.held_block ~= nil
end

function Mouse:getheldblock()
    return self.held_block.block, self.held_block.x, self.held_block.y
end

function Mouse:clearheldblock()
    self.held_block = nil
end

function Mouse:draw(x, y)
    x = x - 32
    y = y - 32
    block_index, _, _ = mouse:getheldblock()
    block_array[block_index]:draw(x, y)
end