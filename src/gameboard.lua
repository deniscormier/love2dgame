-- Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
-- See the file license.txt for copying permission.

local GLOBAL = require("globalvars")
local class = require("lib.30log")
local json = require("lib.dkjson")

GameBoard = class("GameBoard", {
    level_width = 6,
    level_height = 6,
    point_gains = {}
})

function GameBoard:init(level_data_file_name, time_bar, score_bar, mouse)
    -- Game Interface
    self.time_bar = time_bar
    self.score_bar = score_bar

    -- Representation of mouse state
    self.mouse = mouse

    -- Internal representation of the game
    self.game_array = {}
    self.json_data = {}
    --[[ Load the game array
         The columns [x] and rows [y] representation is easier to program
         with because it translates well to the coordinate system ]]
    if love.filesystem.isFile(level_data_file_name) then
        level_file = love.filesystem.newFile(level_data_file_name)  -- Priority is given to C:\Users\(user)\AppData\Roaming\LOVE\FunWithTiles\leveldata.json

        self:readjsondata(level_data_file_name)

        self.level_width = self.json_data.level_width
        self.level_height = self.json_data.level_height
        assert(self.level_width >= 6 and self.level_width <= 12, "The level width must be between 6 and 12")
        assert(self.level_height >= 6 and self.level_height <= 12, "The level height must be between 6 and 12")

        for i = 1, self.level_width do
            self.game_array[i] = {}
        end
        self:loadlevel(1)
    else
        logger:info("'" .. level_data_file_name .. "' was not found, generating random game array...")

        for i = 1, self.level_width do
            self.game_array[i] = {}
        end
        self:loadrandomlevel()
    end

    logger:info("Level width: " .. self.level_width)
    logger:info("Level height: " .. self.level_height)
end

function GameBoard:readjsondata(level_data_file_name)
    logger:info("Reading " .. level_data_file_name .. " from " .. love.filesystem.getSaveDirectory() .. " or from the source directory root")

    level_file:open('r')
    jsondata, jsonsize = level_file:read(all)
    level_file:close()

    logger:debug("Bytes read: " .. jsonsize)
    logger:debug("Data read: \n" .. jsondata)
    self.json_data, pos, err = json.decode(jsondata, 1, nil)
    logger:debug("Position in " .. level_data_file_name .. ": " .. pos)

    if err then
        logger:error("Error while decoding the json file: " .. err)
    else
        logger:info("Total levels read: " .. #self.json_data.levels)
    end


end

function GameBoard:loadlevel(index)
    for i = 1, self.level_width do
        for j = 1, self.level_height do
            self.game_array[i][j] = self.json_data.levels[index][i + (j - 1) * self.level_width]
        end
    end

    assert(table.getn(self.game_array) == self.level_height,
        "The JSON data describing the game array does not conform to level width and/or height")
    for i = 1, self.level_width do
        assert(table.getn(self.game_array[i]) == self.level_width,
            "The JSON data describing the game array does not conform to level width and/or height")
    end
end

function GameBoard:loadrandomlevel()
    for i = 1, self.level_width do
        for j = 1, self.level_height do
            -- A random, non-clear block
            self.game_array[i][j] = math.random(2, #block_array)
        end
    end
end

function GameBoard:draw()
    for i = 1, self.level_width do
        for j = 1, self.level_height do
            if self.game_array[i][j] ~= GLOBAL.INDEX_NO_BLOCK then
                block_array[self.game_array[i][j]]:drawOnBoard(i, j)
            end
        end
    end

    -- Draw the interface last so it lays above everything else
    self.time_bar:draw()
    self.score_bar:draw()

    -- Draw recent point gains (from recently cleared rows)
    self:draw_point_gains()
end

function GameBoard:replaceblock(x_index, y_index, block_index)
    self.game_array[x_index][y_index] = block_index
end

function GameBoard:copyblock(src_x_index, src_y_index, dst_x_index, dst_y_index)
    self.game_array[dst_x_index][dst_y_index] = self.game_array[src_x_index][src_y_index]
end

function GameBoard:scorerowsandcolumns(rows_array, columns_array)
    -- Strategy: go through rows/columns and find consecutive elements

    local positions_to_delete = {}
    local points = {}

    for _, row in ipairs(rows_array) do
        last_block_index = self.game_array[row][1]
        same_index_in_a_row = 1
        for column = 2, level_height do
            if self.game_array[row][column] == last_block_index then
                same_index_in_a_row = same_index_in_a_row + 1
            else
                table.insert(points, GLOBAL.SCORE[same_index_in_a_row])
                last_block_index = self.game_array[row][column]
                same_index_in_a_row = 1
            end
            if same_index_in_a_row == 3 then
                for i = 0, same_index_in_a_row - 1 do
                    table.insert(positions_to_delete, {X = row, Y = column - i})
                end
            elseif same_index_in_a_row >= 4 then
                table.insert(positions_to_delete, {X = row, Y = column})
            end
        end

        if same_index_in_a_row >= 3 then
            table.insert(points, GLOBAL.SCORE[same_index_in_a_row])
        end
    end

    for _, column in ipairs(columns_array) do
        last_block_index = self.game_array[1][column]
        same_index_in_a_row = 1
        for row = 2, level_width do
            if self.game_array[row][column] == last_block_index then
                same_index_in_a_row = same_index_in_a_row + 1
            else
                table.insert(points, GLOBAL.SCORE[same_index_in_a_row])
                last_block_index = self.game_array[row][column]
                same_index_in_a_row = 1
            end
            if same_index_in_a_row == 3 then
                for i = 0, same_index_in_a_row - 1 do
                    table.insert(positions_to_delete, {X = row - i, Y = column})
                end
            elseif same_index_in_a_row >= 4 then
                table.insert(positions_to_delete, {X = row, Y = column})
            end
        end

        if same_index_in_a_row >= 3 then
            table.insert(points, GLOBAL.SCORE[same_index_in_a_row])
        end
    end

    return positions_to_delete, points
end

function GameBoard:scorerows(x1, y1, x2, y2)
    local has_scored = 0

    local rows = {}
    local columns = {}

    table.insert(rows, x1)
    table.insert(columns, y1)

    if (x2 ~= x1) then
        table.insert(rows, x2)
    end
    if (y2 ~= y1) then
        table.insert(columns, y2)
    end

    local positions_to_delete, points = self:scorerowsandcolumns(rows, columns)
    if table.getn(positions_to_delete) > 0 then
        for index, position in pairs(positions_to_delete) do
            self.game_array[position.X][position.Y] = GLOBAL.INDEX_CLEAR_BLOCK
            has_scored = 1
        end
    end

    if has_scored > 0 then
        local sum = 0
        for _, v in ipairs(points) do
            sum = sum + v
        end
        self:insert_in_point_gains(sum)

        mouse:clearheldblock()
        for _, value in pairs(points) do
            self.score_bar:addtobar(value)
        end
        self:spawnblocks()
    end

    return has_scored
end

function GameBoard:getblockindex(x_index, y_index)
    return self.game_array[x_index][y_index]
end

function GameBoard:spawnblocks()
    for i = 1, self.level_width do
        for j = 1, self.level_height do
            if self.game_array[i][j] == GLOBAL.INDEX_CLEAR_BLOCK then
                -- A random, non-clear block
                self.game_array[i][j] = math.random(2, #block_array)
            end
        end
    end
end

-- Take the block under the mouse and draw it to where it would go if a swap were to occur
function GameBoard:draw_preview(x, y)
    x_index = math.floor(x / 64 + 1)
    y_index = math.floor((y - GLOBAL.HUD_HEIGHT) / 64 + 1)
    _, x_held_block, y_held_block = mouse:getheldblock()
    love.graphics.setColor(255, 255, 255, 150)
    block_array[self:getblockindex(x_index, y_index)]:drawOnBoard(x_held_block, y_held_block)
    love.graphics.setColor(255, 255, 255, 255)
end

function GameBoard:countdown()
    table.insert(self.point_gains, {value = 3, duration = 1, time_left = 1})
    table.insert(self.point_gains, {value = 2, duration = 1, time_left = 2})
    table.insert(self.point_gains, {value = 1, duration = 1, time_left = 3})
end

function GameBoard:start_game()
    table.insert(self.point_gains, {value = "Go!", duration = 2, time_left = 2})
end

function GameBoard:game_over()
    table.insert(self.point_gains, {value = "Game", duration = 3, time_left = 3})
    table.insert(self.point_gains, {value = "Over!", duration = 3, time_left = 3})
end

function GameBoard:insert_in_point_gains(points)
    if table.getn(self.point_gains) > GLOBAL.POINT_GAIN_DISPLAY_SLOTS then
        table.remove(self.point_gains, 1)
    end
    table.insert(self.point_gains, {value = "+" .. points, duration = GLOBAL.POINT_GAIN_DISPLAY_DURATION, time_left = GLOBAL.POINT_GAIN_DISPLAY_DURATION})
end

function GameBoard:update_point_gains(dt)
    for _, v in ripairs(self.point_gains) do
        if v["time_left"] <= 0 then
            table.remove(self.point_gains, 1)
        else
            v["time_left"] = v["time_left"] - dt
        end
    end
end

function GameBoard:draw_point_gains()
    love.graphics.setFont(mainFontLarge)
    for i, v in ipairs(self.point_gains) do
        love.graphics.setColor(255, 255, 255, math.min((v["time_left"] / v["duration"]) * 255, 255))
        love.graphics.print(v["value"], (i - 1) * (self.level_width * 64 / GLOBAL.POINT_GAIN_DISPLAY_SLOTS) + 5, 140)
    end
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(mainFont)
end

function ripairs(t)
  local max = 1
  while t[max] ~= nil do
    max = max + 1
  end
  local function ripairs_it(t, i)
    i = i-1
    local v = t[i]
    if v ~= nil then
      return i,v
    else
      return nil
    end
  end
  return ripairs_it, t, max
end