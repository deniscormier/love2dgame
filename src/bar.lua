-- Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
-- See the file license.txt for copying permission.

local GLOBAL = require("globalvars")
local class = require("lib.30log")

Bar = class("Bar", {})

function Bar:init(functionality, name, x_pos, y_pos, img_full, max_units)
    if functionality ~= "TargetScore"
        and functionality ~= "Score"
        and functionality ~= "Timer"
        and functionality ~= "Chrono" then
        logger:error("An invalid functionality '" .. functionality .. "' was assigned to the bar")
    end
    self.img_empty = love.graphics.newImage(GLOBAL.IMAGES_DIR .. "bar_empty.png")
    self.functionality = functionality
    self.name = name
    self.x_pos = x_pos
    self.y_pos = y_pos
    self.img_full = love.graphics.newImage(img_full)
    self.max_units = max_units
    self.units_left = max_units
    self.show_bar = true
    self:setnewquad(1)
end

function Bar:addtobar(amount)
    if self.functionality == "Timer" then
        amount = -amount
    end
    self.units_left = self.units_left + amount

    if self.functionality == "Timer" or self.functionality == "TargetScore" then
        if (self.units_left > self.max_units) then
            self.units_left = self.max_units
        elseif self.units_left < 0 then
            self.units_left = 0
        end
    end

    self:setnewquad(math.min(1, self.units_left / self.max_units))
end

function Bar:isEmpty()
    return self.units_left == 0
end

function Bar:isFull()
    return self.units_left == self.max_units
end

function Bar:setEmpty()
    self.units_left = 0
    -- Set the portion of color that will be drawn in the bar
    self:setnewquad(0)
end

function Bar:setFull()
    self.units_left = self.max_units
    -- Set the portion of color that will be drawn in the bar
    self:setnewquad(1)
end

function Bar:setShowBar(show_bar)
    self.show_bar = show_bar
end

function Bar:setFunctionality(func)
    if func ~= "TargetScore" and func ~= "Score" and func ~= "Timer" and func ~= "Chrono" then
        logger:error("An invalid functionality '" .. func .. "' was assigned to the bar")
    end
    self.functionality = func
end

-- Set the portion of color that will be drawn in the bar
function Bar:setnewquad(fraction)
    -- Set the portion of color that will be drawn in the bar
    self.units_displayed = love.graphics.newQuad(8, 0, fraction * 200, 28, 214, 28)
end

function Bar:draw()
    love.graphics.print(self.name, self.x_pos, self.y_pos)

    if self.show_bar then
        love.graphics.draw(self.img_empty, self.x_pos + 48, self.y_pos)
        love.graphics.drawq(self.img_full, self.units_displayed, self.x_pos + 56, self.y_pos)
    end

    if self.functionality == "TargetScore" then
        love.graphics.print(self.units_left.."/"..self.max_units, self.x_pos + 273, self.y_pos)
    elseif self.functionality == "Score" then
        love.graphics.print(self.units_left, self.x_pos + 273, self.y_pos)
    elseif self.functionality == "Timer" or self.functionality == "Chrono" then
        -- Figure out if a leading zero has to be printed (done if seconds < 10)
        local seconds = self.units_left % 60
        local leading_zero = ""
        if (seconds < 10) then
            leading_zero = "0"
        end
        love.graphics.print(math.floor(self.units_left / 60) .. ":" .. leading_zero .. math.floor(seconds * 100) / 100, self.x_pos + 273, self.y_pos);
    end
end
