-- Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
-- See the file license.txt for copying permission.

local GLOBAL = require("globalvars")
local class = require("lib.30log")

Square = class("Square", {
    width = 256,
    height = 256
})

function Square:init(imagefile)
    self.img = love.graphics.newImage(imagefile)
end

function Square:drawOnBoard(x, y)
    love.graphics.draw(self.img, x * 64 - 64, y * 64 - 64 + GLOBAL.HUD_HEIGHT, 0, 2)
end

function Square:draw(x, y)
    love.graphics.draw(self.img, x, y, 0, 2)
end
