-- Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
-- See the file license.txt for copying permission.

-- Fun With Tiles!
-- For LOVE version 0.8.0 by Denis Cormier

-- For now, explicitly declaring variables will hinder my flexibility
-- require "lib.strict"
local GLOBAL = require("globalvars")
require("bar")
require("square")
require("logger")
require("mouse")
require("gameboard")
require("highscorewrapper")

require("lib.loveframes")  -- Can't use local variable since the package does not return the table (interface)
require("lib.TEsound")

local time_accumulator = 0;
local game_in_progress = false
local previous_state = "none"

function love.load()
    -- When requested, start the debugger
    if arg[#arg] == "-debug" then require("mobdebug").start() end

    logger = Logger:new("DEBUG", "debug.log")
    logger:info("Using save directory: " .. love.filesystem.getSaveDirectory())  -- Expecting C:\Users\(user)\AppData\Roaming\LOVE\Tilegame on Windows

    if love._version:find("^0%.[0-7]%.") then -- if version < 0.8.0
        logger:warning("This game was made for LOVE version 0.8.0, running LOVE version " .. love._version)
    end

    -- Set font
    mainFont = love.graphics.newFont(GLOBAL.FONTS_DIR .. "Averia-Regular.ttf", 16);
    mainFontLarge = love.graphics.newFont(GLOBAL.FONTS_DIR .. "Averia-Regular.ttf", 32);

    -- Instantiate png images
    background = love.graphics.newImage(GLOBAL.IMAGES_DIR .. "seamless_space.png")
    block_array = {
        Square:new(GLOBAL.IMAGES_DIR .. "element_clear.png"),
        Square:new(GLOBAL.IMAGES_DIR .. "element_blue_square.png"),
        Square:new(GLOBAL.IMAGES_DIR .. "element_green_square.png"),
        Square:new(GLOBAL.IMAGES_DIR .. "element_purple_square.png"),
        Square:new(GLOBAL.IMAGES_DIR .. "element_red_square.png"),
        Square:new(GLOBAL.IMAGES_DIR .. "element_yellow_square.png")
    }

    -- Instantiate on-screen objects
    time_bar = Bar:new("Timer", "Time", 32, 30, GLOBAL.IMAGES_DIR .. "bar_red.png", GLOBAL.MAX_TIME)  -- in seconds
    score_bar = Bar:new("TargetScore", "Score", 32, 50, GLOBAL.IMAGES_DIR .. "bar_blue.png", GLOBAL.MAX_SCORE)  -- Track the score
    mouse = Mouse:new() -- Representation of the mouse's state when holding/releasing blocks
    game_board = GameBoard:new("leveldata.json", time_bar, score_bar, mouse)
    level_width = game_board.level_width
    level_height = game_board.level_height

    high_score_wrapper = HighScoreWrapper:new()
    high_score_wrapper:addfile("highscore_top_scores.txt", "Score", 10, "Default", {
        1000, 900, 800, 700, 600, 500, 400, 300, 200, 100
    })

    high_score_wrapper:addfile("highscore_top_times.txt", "Time", 10, "Default", {
        600, 660, 720, 780, 840, 900, 960, 1020, 1080, 1140
    })

    --[[--------------------------------------
        Loveframes GUI library initializations
    --]]--------------------------------------

    -- Main menu
    start_game_menu_button = loveframes.Create("button")
    start_game_menu_button:SetPos(10, 85)
    start_game_menu_button:SetText("Start Game")
    start_game_menu_button.OnClick = function(object)
        loveframes.SetState("mode_selection")
    end
    start_game_menu_button:SetState("main_menu")

    how_to_play_menu_button = loveframes.Create("button")
    how_to_play_menu_button:SetPos(100, 85)
    how_to_play_menu_button:SetText("How to Play")
    how_to_play_menu_button.OnClick = function(object)
        previous_state = loveframes.GetState()
        loveframes.SetState("how_to_play")
    end
    how_to_play_menu_button:SetState("main_menu")

    high_score_menu_button = loveframes.Create("button")
    high_score_menu_button:SetPos(190, 85)
    high_score_menu_button:SetText("High Scores")
    high_score_menu_button.OnClick = function(object)
        loveframes.SetState("high_score")
    end
    high_score_menu_button:SetState("main_menu")

    quit_menu_button = loveframes.Create("button")
    quit_menu_button:SetPos(280, 85)
    quit_menu_button:SetText("Quit")
    quit_menu_button.OnClick = function(object)
        love.event.push("quit")
    end
    quit_menu_button:SetState("main_menu")

    -- Mode selection
    time_attack_button = loveframes.Create("button")
    time_attack_button:SetPos(10, 85)
    time_attack_button:SetText("Time Attack")
    time_attack_button.OnClick = function(object)
        GLOBAL.HIGH_SCORE_MODE  = "Time Attack"
        time_bar:setShowBar(true)
        score_bar:setShowBar(false)
        time_bar:setFunctionality("Timer")
        score_bar:setFunctionality("Score")
        score_bar:setEmpty()
        time_bar:setFull()
        game_board:loadrandomlevel()
        game_board:countdown()
        loveframes.SetState("countdown")
    end
    time_attack_button:SetState("mode_selection")

    score_attack_button = loveframes.Create("button")
    score_attack_button:SetPos(100, 85)
    score_attack_button:SetText("Score Attack")
    score_attack_button.OnClick = function(object)
        GLOBAL.HIGH_SCORE_MODE  = "Score Attack"
        score_bar:setShowBar(true)
        time_bar:setShowBar(false)
        time_bar:setFunctionality("Chrono")
        score_bar:setFunctionality("TargetScore")
        score_bar:setEmpty()
        time_bar:setEmpty()
        game_board:loadrandomlevel()
        game_board:countdown()
        loveframes.SetState("countdown")
    end
    score_attack_button:SetState("mode_selection")

    -- In-game options
    pause_game_button = loveframes.Create("button")
    pause_game_button:SetPos(10, 85)
    pause_game_button:SetText("Pause Game")
    pause_game_button.OnClick = function(object)
        loveframes.SetState("pause_menu")
    end
    pause_game_button:SetState("play")

    -- Pause menu
    resume_game_button = loveframes.Create("button")
    resume_game_button:SetPos(10, 85)
    resume_game_button:SetText("Resume Game")
    resume_game_button.OnClick = function(object)
        loveframes.SetState("play")
    end
    resume_game_button:SetState("pause_menu")

    how_to_play_game_button = loveframes.Create("button")
    how_to_play_game_button:SetPos(100, 85)
    how_to_play_game_button:SetText("How to Play")
    how_to_play_game_button.OnClick = function(object)
        previous_state = loveframes.GetState()
        loveframes.SetState("how_to_play")
    end
    how_to_play_game_button:SetState("pause_menu")

    exit_game_button = loveframes.Create("button")
    exit_game_button:SetPos(190, 85)
    exit_game_button:SetText("Exit to menu")
    exit_game_button.OnClick = function(object)
        loveframes.SetState("main_menu")
    end
    exit_game_button:SetState("pause_menu")

    quit_game_button = loveframes.Create("button")
    quit_game_button:SetPos(280, 85)
    quit_game_button:SetText("Quit")
    quit_game_button.OnClick = function(object)
        love.event.push("quit")
    end
    quit_game_button:SetState("pause_menu")

    -- How to play pop-up
    how_to_play_menu_frame = loveframes.Create("frame")
    how_to_play_menu_frame:SetSize(380, 300)
    how_to_play_menu_frame:Center()
    how_to_play_menu_frame:SetName("How to Play")

    how_to_play_menu_frame:ShowCloseButton(true)
    how_to_play_menu_frame:SetDraggable(false)
    how_to_play_menu_frame:ShowCloseButton(false)
    how_to_play_menu_frame:SetState("how_to_play")

    how_to_play_menu_text = loveframes.Create("text", how_to_play_menu_frame)
    how_to_play_menu_text:SetPos(2, 28)
    how_to_play_menu_text:SetMaxWidth(380)
    how_to_play_menu_text:SetText(GLOBAL.HOW_TO_PLAY_STRING)
    how_to_play_menu_text:SetState("how_to_play")

    how_to_play_menu_text2 = loveframes.Create("text", how_to_play_menu_frame)
    how_to_play_menu_text2:SetPos(2, 84)
    how_to_play_menu_text2:SetMaxWidth(380)
    how_to_play_menu_text2:SetText(GLOBAL.TIME_ATTACK_STRING)
    how_to_play_menu_text2:SetState("how_to_play")

    how_to_play_menu_text3 = loveframes.Create("text", how_to_play_menu_frame)
    how_to_play_menu_text3:SetPos(2, 140)
    how_to_play_menu_text3:SetMaxWidth(380)
    how_to_play_menu_text3:SetText(GLOBAL.SCORE_ATTACK_STRING)
    how_to_play_menu_text3:SetState("how_to_play")

    how_to_play_menu_close_button = loveframes.Create("button", how_to_play_menu_frame)
    how_to_play_menu_close_button:SetPos(290, 265)
    how_to_play_menu_close_button:SetText("Close")
    how_to_play_menu_close_button.OnClick = function(object)
        loveframes.SetState(previous_state)
    end
    how_to_play_menu_close_button:SetState("how_to_play")

    -- High score screen
    high_score_mode_button = loveframes.Create("button")
    high_score_mode_button:SetPos(10, 85)
    if GLOBAL.HIGH_SCORE_MODE == "Score Attack" then
        high_score_mode_button:SetText("Time Attack")
    else
        high_score_mode_button:SetText("Score Attack")
    end
    high_score_mode_button.OnClick = function(object)
        high_score_mode_button:SetText(GLOBAL.HIGH_SCORE_MODE)
        if GLOBAL.HIGH_SCORE_MODE == "Score Attack" then
            GLOBAL.HIGH_SCORE_MODE = "Time Attack"
        else
            GLOBAL.HIGH_SCORE_MODE = "Score Attack"
        end
    end
    high_score_mode_button:SetState("high_score")

    exit_high_score_button = loveframes.Create("button")
    exit_high_score_button:SetPos(100, 85)
    exit_high_score_button:SetText("Exit to menu")
    exit_high_score_button.OnClick = function(object)
        loveframes.SetState("main_menu")
    end
    exit_high_score_button:SetState("high_score")

    -- New high score popup
    new_high_score_frame = loveframes.Create("frame")
    new_high_score_frame:SetSize(380, 300)
    new_high_score_frame:Center()
    new_high_score_frame:SetName("New high score!")

    new_high_score_frame:ShowCloseButton(true)
    new_high_score_frame:SetDraggable(false)
    new_high_score_frame:ShowCloseButton(false)
    new_high_score_frame:SetState("new_high_score")

    new_high_score_text = loveframes.Create("text", new_high_score_frame)
    new_high_score_text:SetPos(2, 28)
    new_high_score_text:SetMaxWidth(380)
    new_high_score_text:SetText(GLOBAL.NEW_HIGH_SCORE_STRING)
    new_high_score_text:SetState("new_high_score")

    new_high_score_text2 = loveframes.Create("text", new_high_score_frame)
    new_high_score_text2:SetPos(2, 100)
    new_high_score_text2:SetMaxWidth(380)
    new_high_score_text2:SetText(GLOBAL.ENTER_NAME_HERE_STRING)
    new_high_score_text2:SetState("new_high_score")

    new_high_score_textinput = loveframes.Create("textinput", new_high_score_frame)
    new_high_score_textinput:SetPos(114, 95)
    new_high_score_textinput:SetState("new_high_score")

    new_high_score_close_button = loveframes.Create("button", new_high_score_frame)
    new_high_score_close_button:SetPos(290, 265)
    new_high_score_close_button:SetText("Close")
    new_high_score_close_button.OnClick = function(object)
        if GLOBAL.HIGH_SCORE_MODE == "Time Attack" then
            high_score_wrapper:add("Score", new_high_score_textinput:GetText(), score_bar.units_left)
        elseif GLOBAL.HIGH_SCORE_MODE == "Score Attack" then
            high_score_wrapper:add("Time", new_high_score_textinput:GetText(), time_bar.units_left)
        end
        loveframes.SetState("high_score")
    end
    new_high_score_close_button:SetState("new_high_score")

    loveframes.SetState("main_menu")  -- Set state at the start of the program
end

function love.update(dt)
    -- Note: Accumulating values of dt will add up to 1 per second

    if loveframes.GetState() == "countdown" then
        if time_accumulator > 3 then
            time_accumulator = 0
            game_in_progress = true
            game_board:start_game()
            loveframes.SetState("play")
        else
            time_accumulator = time_accumulator + dt
        end
    end

    if loveframes.GetState() == "play" and game_in_progress then
        time_bar:addtobar(dt)
        if GLOBAL.HIGH_SCORE_MODE  == "Time Attack" and time_bar:isEmpty()
            or GLOBAL.HIGH_SCORE_MODE  == "Score Attack" and score_bar:isFull() then
            --TEsound.play(GLOBAL.SOUNDS_DIR .. "Sound effect - Wha Wha!.mp3")
            TEsound.play(GLOBAL.SOUNDS_DIR .. "Kids Saying Yay! Sound Effect.mp3")
            game_in_progress = false;
            game_board:game_over()
            loveframes.SetState("game_over")
        end
    end

    if loveframes.GetState() == "game_over" then
        if time_accumulator > 3 then
            time_accumulator = 0
            if (GLOBAL.HIGH_SCORE_MODE == "Time Attack" and high_score_wrapper:ishighscore("Score", score_bar.units_left))
                    or (GLOBAL.HIGH_SCORE_MODE == "Score Attack" and high_score_wrapper:ishighscore("Time", time_bar.units_left)) then
                loveframes.SetState("new_high_score")
            else
                loveframes.SetState("main_menu")
            end
        else
            time_accumulator = time_accumulator + dt
        end
    end

    -- Even when we pause, process the point gains so they disappear from the interface
    game_board:update_point_gains(dt)

    loveframes.update(dt)
end

function love.draw()
    love.graphics.draw(background, 0, 0)

    love.graphics.setFont(mainFont)

    if loveframes.GetState() == "mode_selection" then
        love.graphics.setFont(mainFontLarge)
        love.graphics.print("Choose mode", 20, 130);
        love.graphics.setFont(mainFont)
    end

    if loveframes.GetState() == "high_score" then
        ---[[
        if GLOBAL.HIGH_SCORE_MODE == "Score Attack" then
            high_score_wrapper:draw("Time")
        else
            high_score_wrapper:draw("Score")
        end
        --]]
    else
        game_board:draw()

        --[[
        If left click and held block is not 0 (representing no block)
            draw it
        ]]
        if love.mouse.isDown("l") and mouse:isholdingblock() then
            -- Draw the swap preview first
            game_board:draw_preview(love.mouse.getPosition())
            mouse:draw(love.mouse.getPosition())
        end
    end
    loveframes.draw()
end

function love.mousepressed(x, y, button)
    --[[
    If left click, in play area, and state = play
        keep track of block under cursor (held block)
        copy clear block where the held block used to be
    ]]
    --logger:debug("x: " .. x .. ", y: " .. y .. ", button: " .. button .. " (pressed)")

    TEsound.play(GLOBAL.SOUNDS_DIR .. "mouseclick1.wav")
    if button == "l" and y >= GLOBAL.HUD_HEIGHT and loveframes.GetState() == "play" and game_in_progress then
        x_index = math.floor(x / 64 + 1)
        y_index = math.floor((y - GLOBAL.HUD_HEIGHT) / 64 + 1)
        mouse:holdblock(game_board:getblockindex(x_index, y_index), x_index, y_index)
        game_board:replaceblock(x_index, y_index, GLOBAL.INDEX_CLEAR_BLOCK)
    end

    loveframes.mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
    --[[
    If left click
        if in play area, and state = play, and the mouse is holding a block
            swap block = block under cursor at this point
            copy swap block where the held block used to be
            copy held block in game array location under mouse
            score rows of 3 or more
            if scoring rows did not get us points
                copy swap block (held block's old location) to game array location under mouse
                copy held block to its original location
        else
            if holding a block
                return held block to its original position
        clear held block from mouse

    ]]
    --logger:debug("x: " .. x .. ", y: " .. y .. ", button: " .. button .. " (released)")
    --TEsound.play(GLOBAL.SOUNDS_DIR .. "mouserelease1.wav")
    if button == "l" then
        if y >= GLOBAL.HUD_HEIGHT and mouse:isholdingblock() and loveframes.GetState() == "play" then
            x_index_under_mouse = math.floor(x / 64 + 1)
            y_index_under_mouse = math.floor((y - GLOBAL.HUD_HEIGHT) / 64 + 1)
            held_block_index, x_held_block, y_held_block = mouse:getheldblock()
            game_board:copyblock(x_index_under_mouse, y_index_under_mouse, x_held_block, y_held_block)
            game_board:replaceblock(x_index_under_mouse, y_index_under_mouse, held_block_index)
            score_earned = game_board:scorerows(x_index_under_mouse, y_index_under_mouse, x_held_block, y_held_block)
            if score_earned == 0 then
                game_board:copyblock(x_held_block, y_held_block, x_index_under_mouse, y_index_under_mouse)
                game_board:replaceblock(x_held_block, y_held_block, held_block_index)
            else
                TEsound.play(GLOBAL.SOUNDS_DIR .. "spin_jump-Brandino480-2020916281.mp3")
            end
        else
            if mouse:isholdingblock() then
                held_block_index, x_held_block, y_held_block = mouse:getheldblock()
                game_board:replaceblock(x_held_block, y_held_block, held_block_index)
            end
        end
        mouse:clearheldblock()
    else
        --TODO: Implement right-click functionality
    end

    loveframes.mousereleased(x, y, button)
end

function love.keypressed(key, unicode)
    if key == "escape" then
        love.event.push("quit") -- actually causes the app to quit
    end

    if key == "p" then
        if loveframes.GetState() == "play" then
            loveframes.SetState("pause_menu")
        elseif loveframes.GetState() == "pause_menu" then
            loveframes.SetState("play")
        end
    end

    loveframes.keypressed(key, unicode)
end

function love.keyreleased(key)
    loveframes.keyreleased(key)
end

function love.quit()
    logger:warning("Quitting the game.")
    logger:close()
end