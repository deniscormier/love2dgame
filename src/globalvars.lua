-- Create global variables that won't pollute the _G table
local GLOBAL = {
    INDEX_NO_BLOCK = 0,
    INDEX_CLEAR_BLOCK = 1,
    HOW_TO_PLAY_STRING = [[
Use the mouse (left button) to drag squares and make lines of 3 same squares.
]],
    TIME_ATTACK_STRING = [[
In time attack, your goal is to maximize your score within the time limit.
]],
    SCORE_ATTACK_STRING = [[
In score attack, your goal is to score a set amount of points as fast as possible.
]],
    NEW_HIGH_SCORE_STRING = [[
Congratulations! You have just set a new high score!
]],
    ENTER_NAME_HERE_STRING = [[
Enter your name:
]],
    MAX_TIME = 120,
    MAX_SCORE = 2000,
    SCORE = {nil, nil, 30, 45, 70, 110},
    FONTS_DIR = "fonts/",
    IMAGES_DIR = "images/",
    SOUNDS_DIR = "sounds/",
    HUD_HEIGHT = 192,
    POINT_GAIN_DISPLAY_SLOTS = 4,
    POINT_GAIN_DISPLAY_DURATION = 2,
    HIGH_SCORE_MODE = "Score Attack"
}
return GLOBAL