-- Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
-- See the file license.txt for copying permission.

local class = require("lib.30log")

Logger = class("Logger", {
    level = "WARNING"
})

local logging_level = {
    DEBUG = {rank = 1, letter = "D"},
    INFO = {rank = 2, letter = "I"},
    WARNING = {rank = 3, letter = "W"},
    ERROR = {rank = 4, letter = "E"}
}

function Logger:init(level, file_name)

    if level ~= nil then
        self.level = level;
    end

    if file_name == nil then
        self.file = love.filesystem.newFile("debug.log")
    else
        self.file = love.filesystem.newFile(file_name)
    end

    self.file:open('w')
end

function Logger:debug(message)
    self:write("DEBUG", message)
end

function Logger:info(message)
    self:write("INFO", message)
end

function Logger:warning(message)
    self:write("WARNING", message)
end

function Logger:error(message)
    self:write("ERROR", message)
end

function Logger:write(level, message)
    if logging_level[level].rank >= logging_level[self.level].rank then
        self.file:write(logging_level[level].letter .. " " .. os.date("%Y-%m-%d %X", os.time())
            .. " " .. message .. "\n")
    end
end

function Logger:close()
    self.file:close()
end