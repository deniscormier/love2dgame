# Project: Fun With Tiles (a match-3 game)

See the design document for more information about the game.

## How to play

Want to try the game out?

1. Obtain the .love package of this game [here](https://bitbucket.org/deniscormier/love2dgame/downloads)
2. Obtain a copy of Love 0.8.0 [here](https://bitbucket.org/rude/love/downloads)
3. Drag the love package on the love executable (or double-click the package if love 0.8.0 is installed on your system)
4. Play!

## License

The usage of my code falls under the terms of the MIT/X11 license (see the LICENSE file or see http://opensource.org/licenses/MIT) except otherwise noted.
However, the files under the lib directory do not belong to me. They operate under their own license or are posted on Love2D's website under no license.

## Prerequisites

- Love 0.8.0
    - This is the game engine. The lua code is interpreted by this executable
    - You may install Love on your computer or download the executable in a known location with its associated DLLs
- 7-zip command-line executable
    - Allows for file archiving in order to package the Lua source code
- lib files
    - loveframes
        - GUI library (a bit overkill for what I end up using this for ha ha)
        - http://nikolairesokav.com/documents/?page=loveframes/main
    - 30log
        - Allow object-oriented concepts
        - https://github.com/Yonaba/30log
    - dkjson.lua
        - JSON parser
        - http://dkolf.de/src/dkjson-lua.fsl/home
    - sick.lua
        - High score functionality
        - https://love2d.org/wiki/SICK
    - TEsound.lua
        - Convenience wrapper for playing sounds
        - https://love2d.org/wiki/TEsound

## Packaging
There's no compilation occuring. Lua, the scripting language for Love2d, is an interpreted language. The necessary
Love2d binaries are already compiled. The lua code simply has to be packaged properly.

- Execute package.bat in cmd.exe or double-click it from the desktop. This will...
    - zip the folder with all your Lua scripts and any other necessary files (*.zip)
    - Change the zip folder's extension to ".love"

## Execution
- If you have Love installed, execute the game.love package to run the game
- If not, you can drag the package on a Love executable to run the package
